# GeoTIFF Conversion

This project provides a sample conversion of a netCDF4 file to GeoTIFF along with information on then converting that GeoTIFF to a Cloud Optimized GeoTIFF. 

The provided python script (SampleGeoTiffConverter.py) reads a sample L3 monthly NO2 file and then writes the data to a GeoTIFF file. The script can be easily adpated to other applications by simply writing a reader for your own data and providing the correct lats and lons for your own data. 

In order to create a Cloud Optimized GeoTIFF files, we take advantage of the tool rio-cogeo which can be easily installed with anaconda (please see https://github.com/cogeotiff/rio-cogeo for further instructions).

To use rio-cogeo and create a Cloud Optimized GeoTIFF run the command:

`rio cogeo create my_raster.tif my_cog_raster.tif --nodata 9.969209968386869e+3`

Note the --nodata value provides the file with your fill value. If you included this metadata value in creating your GeoTIFF then you do not need to use that option in rio-cogeo. 

For information on filename standards for use in NASA Dashboard, please see https://github.com/NASA-IMPACT/covid-api/blob/master/guidelines/raster.md. 
