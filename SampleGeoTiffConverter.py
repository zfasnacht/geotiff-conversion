from netCDF4 import Dataset
from osgeo import gdal
from osgeo import gdal_array
from osgeo import osr
import numpy as np

#Reading in Tropospheric NO2 from OMI 
f = Dataset('OMI_trno2_0.10x0.10_202006_Col3_V4.nc','r')
no2_2020 = np.array(f['TroposphericNO2'][:])

#Switching lat dimension so that north is up
no2_2020 = no2_2020[::-1,:]

#Creating lat and lon information for GeoTIFF
x_len = len(no2_2020[:,0])
y_len = len(no2_2020[0,:])
lat = np.repeat(np.linspace(-90,90,x_len)[:,np.newaxis],y_len,-1)
lon = np.repeat(np.linspace(-180,180,y_len)[np.newaxis,:],x_len,0)

#Specifying GeoTIFF Format
format = "GTiff"

#Specifying GeoTIFF coordinate bounds
xmin,ymin,xmax,ymax = [lon.min(),lat.min(),lon.max(),lat.max()]
nrows,ncols = np.shape(no2_2020)
xres = (xmax-xmin)/float(ncols)
yres = (ymax-ymin)/float(nrows)

# That's (top left x, w-e pixel resolution, rotation (0 if North is up), 
#         top left y, rotation (0 if North is up), n-s pixel resolution)
geotransform=(xmin,xres,0,ymax,0, -yres)   



output_raster = gdal.GetDriverByName('GTiff').Create('OMI_trno2_0.10x0.10_202006_Col3_V4.nc.tiff',ncols, nrows, 1 ,gdal.GDT_Float32)  # Open the file
output_raster.SetGeoTransform(geotransform)  # Specify its coordinates
srs = osr.SpatialReference()                 # Establish its coordinate encoding
srs.ImportFromEPSG(4326)                     # This one specifies WGS84 lat long.
output_raster.SetProjection( srs.ExportToWkt() )   # Exports the coordinate system to the file
output_raster.GetRasterBand(1).WriteArray(no2_2020)   # Writes my array to the raster

output_raster.FlushCache()

